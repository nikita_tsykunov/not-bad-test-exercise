In order to start project run `docker-compose docker-compose.yml`. 
Then service can be opened in browser by <http://localhost:3030>

Tests are running by `npm run test` in corresponding folder.

Frontend consist of table with current game. New game is generated randomly and sent to server after clicking "Add game" button. It will first appear in "Pending games" table and after 10 seconds will appear in general table.
