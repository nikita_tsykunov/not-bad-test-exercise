import React, { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import { addGame, createRandomGame, Game, loadGames, subscribeToGames } from './services/casino';
import './app.css';

function App(): JSX.Element {
    const [games, setGames] = useState<Game[]>([]);
    const [pendingGames, setPendingGames] = useState<Game[]>([]);
    const [showAlert, setShowAlert] = useState<string>();

    useEffect(() => {
        function handleMessage({ data }: MessageEvent): void {
            const newGame = JSON.parse(data.toString());
            setGames((prevGames) => {
                return [...prevGames, newGame];
            });
            setPendingGames(pendingGames.filter((game) => game.id !== newGame.id));
        }

        function handleClose(): void {
            setShowAlert('close');
            subscribeToGames(handleMessage, () => setShowAlert('open'), handleClose);
            setTimeout(() => setShowAlert(undefined), 2000);
        }

        if (!games || !games.length) {
            loadGames().then(setGames);
            subscribeToGames(handleMessage, handleOpen, handleClose);
        }
    }, [games, pendingGames]);

    async function addNewGame(): Promise<void> {
        addGame(createRandomGame()).then((newGame) => setPendingGames([...pendingGames, newGame]));
    }

    function handleOpen(): void {
        setShowAlert('open');
        setTimeout(() => setShowAlert(undefined), 2000);
    }

    return games && games.length ? (
        <>
            <Table bordered striped hover>
                <thead>
                    <tr>
                        {Object.keys(games[0]).map((key) => (
                            <th key={key}>{key}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {games.map((game) => (
                        <tr key={game.id}>
                            {Object.entries(game).map(([key, value]) => (
                                <td key={key}>{value}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </Table>
            <Button variant="primary" onClick={addNewGame}>
                Add Game
            </Button>
            <h2>Pending Games</h2>
            <Table bordered striped hover>
                <thead>
                    <tr>
                        {Object.keys(games[0]).map((key) => (
                            <th key={key}>{key}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {pendingGames.map((game) => (
                        <tr key={game.id}>
                            {Object.entries(game).map(([key, value]) => (
                                <td key={key}>{value}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </Table>
            <div className="fixed">
                <Alert show={showAlert === 'open'} variant="success">
                    Websocket is connected!
                </Alert>
                <Alert show={showAlert === 'close'} variant="danger">
                    'Websocket is closed. Reconnecting...
                </Alert>
            </div>
        </>
    ) : (
        <Spinner animation="border" variant="primary" data-testid="spinner-border" />
    );
}

export default App;
