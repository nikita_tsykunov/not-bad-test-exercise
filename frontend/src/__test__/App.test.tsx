import React from 'react';
import App from '../App';
import { fireEvent, render, wait } from '@testing-library/react';
import fetchMock from 'jest-fetch-mock';
import WS from 'jest-websocket-mock';

fetchMock.enableMocks();
describe('Testing games page', () => {
    beforeEach(() => {
        fetchMock.resetMocks();
    });

    describe('When there are no games fetched', () => {
        it('Then it should render loading', async () => {
            fetchMock.once(JSON.stringify([]));
            const { findByTestId } = render(<App />);
            await wait(() => findByTestId('spinner-border'));
        });
    });
    describe('When games are fetched', () => {
        it('Then it should display table', async () => {
            fetchMock.once(
                JSON.stringify([
                    {
                        id: 1,
                        description: 'Description',
                        name: 'Test Name',
                        provider: 'Test Provider',
                        imagePath: 'path',
                    },
                ]),
                {
                    status: 200,
                    headers: { 'Content-type': 'application/json' },
                },
            );
            const { findByText } = render(<App />);
            await wait(() => findByText('Test Name'));
        });
        describe('When button is clicked', () => {
            it('Then it should add new game', async () => {
                fetchMock.once(
                    JSON.stringify([
                        {
                            id: 1,
                            description: 'Description',
                            name: 'Test Name',
                            provider: 'Test Provider',
                            imagePath: 'path',
                        },
                    ]),
                    {
                        status: 200,
                        headers: { 'Content-type': 'application/json' },
                    },
                );
                const server = new WS('ws://localhost:3000/ws/games');
                const { findByRole, findByText } = render(<App />);
                const button = await findByRole('button');

                fetchMock.once(() => {
                    server.send(
                        JSON.stringify({
                            id: 1,
                            description: 'Description',
                            name: 'Test Name 2',
                            provider: 'Test Provider',
                            imagePath: 'path',
                        }),
                    );
                    return new Promise((resolve) =>
                        resolve({
                            body: JSON.stringify({
                                id: 1,
                                description: 'Description',
                                name: 'Test Name 2',
                                provider: 'Test Provider',
                                imagePath: 'path',
                            }),
                        }),
                    );
                });
                fireEvent.click(button);
                await wait(async () => findByText('Test Name 2'));
            });
        });
    });
});
