import { LoremIpsum } from 'lorem-ipsum';

export type Game = {
    id: number;
    name: string;
    description?: string;
    provider: string;
    imagePath: string;
    createdAt?: Date;
    updatedAt?: Date;
};

const serviceUrl = 'http://localhost:3000';
const headers = {
    'Content-Type': 'application/json; charset=utf-8',
};

const gameNames = [
    'Monkey Warrior',
    'Master Joker',
    'African Quest',
    'Joker Strike',
    'Dreamzone',
    'Mysterious',
    'Dragon Sisters',
    'King of 3 Kingdoms',
    'Speed Blackjack',
    'Aztec Idols',
    'Chinese New Year',
    'Inferno Star',
    'Arthur’s Fortune',
    'Wild North',
    'Nero’s Fortune',
    'Danger High Voltage',
];

const providerNames = [
    'Yggdrasil',
    "Play'n GO",
    'Netent Live',
    'Evolution',
    'ELK Studios',
    'Netent',
    'Hub 88',
    'Microgaming',
];

const lorem = new LoremIpsum();

export let ws = new WebSocket(`ws://localhost:3000/ws/games`);

export async function loadGames(): Promise<Game[]> {
    return fetch(`${serviceUrl}/games`, { method: 'GET', headers })
        .then((result) => result.json())
        .catch(console.error);
}

export async function addGame(game: Partial<Game>): Promise<Game> {
    return fetch(`${serviceUrl}/games`, { method: 'POST', body: JSON.stringify(game), headers })
        .then((result) => result.json())
        .catch(console.error);
}

export function createRandomGame(): Partial<Game> {
    const gameName = gameNames[Math.floor(Math.random() * gameNames.length)];

    return {
        name: gameName,
        description: lorem.generateSentences(1),
        provider: providerNames[Math.floor(Math.random() * providerNames.length)],
        imagePath: `/s/images/${gameName.toLowerCase().replace("'", '').replace(' ', '_')}.jpg`,
    };
}

export function subscribeToGames(
    onMessage: (message: MessageEvent) => void,
    onOpen?: () => void,
    onClose?: (event: CloseEvent) => void,
): void {
    ws.addEventListener('message', onMessage);
    ws.addEventListener('open', () => {
        console.log('WebSocket connection is opened!');
        onOpen && onOpen();
    });
    ws.addEventListener('close', (event: CloseEvent) => {
        console.log(`WebSocket connection was closed with following reason: ${event.reason} ${event.code}`);
        ws = new WebSocket('ws://localhost:3000/ws/games');
        onClose && onClose(event);
    });
}
