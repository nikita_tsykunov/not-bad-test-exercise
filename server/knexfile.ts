export = {
    dev: {
        client: 'postgresql',
        connection: {
            database: 'casino',
            user: 'postgres',
            password: 'postgres',
            port: 5432,
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: 'src/database/migrations',
        },
        seeds: {
            directory: 'src/database/seeds',
        },
        debug: true,
    },

    production: {
        client: 'postgresql',
        connection: {
            database: 'casino',
            user: 'postgres',
            password: 'postgres',
            port: 54321,
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: 'src/database/migrations',
        },
        seeds: {
            directory: 'src/database/seeds',
        },
    },
};
