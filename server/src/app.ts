import express, { Express } from 'express';
import { createDatabase, runMigrations, runSeeds } from './database/connection';
import { errorHandler } from './middleware/errorHandler';
import { createGame, deleteGame, getGameById, getGames, updateGame } from './controllers/games';
import { createServer, Server } from 'http';
import { Server as WebSocketServer } from 'ws';
import cors from 'cors';

(async () => {
    if (process.env.NODE_ENV !== 'test') {
        await createDatabase();
        await runMigrations();
        await runSeeds();
        setupExpress();
    }
})();

export let wss: WebSocketServer;

export function setupExpress(onListen?: (value?: unknown) => void): Server {
    const app = express();
    const server = createServer(app);
    const whitelist = ['http://localhost:3001', 'http://localhost'];

    app.use(
        cors({
            origin: (origin, cb) => {
                return whitelist.includes(origin) || !origin ? cb(null, true) : cb(new Error('Not allowed by CORS'));
            },
        }),
    );
    app.use(express.json());
    setupEndpoints(app);
    app.use(errorHandler);

    setupWebsocket(server);

    return server.listen(3000, '0.0.0.0', () => {
        console.log(`App has started at address ${JSON.stringify(server.address())}`);
        onListen && onListen();
    });
}

function setupWebsocket(server: Server): void {
    wss = new WebSocketServer({ server, path: '/ws/games' });

    const interval = setInterval(() => {
        wss.clients.forEach((ws) => {
            if (ws['isAlive'] === false) {
                return ws.terminate();
            }

            ws['isAlive'] = false;
            ws.ping();
        });
    }, 30000);

    wss.on('connection', (ws, req) => {
        console.log('WebSocket is connected', req.socket.address());
        ws['isAlive'] = true;
        ws.on('pong', () => (ws['isAlive'] = true));
        ws.addEventListener('close', ({ reason }) =>
            console.log(`Connection is closed because of following reason: ${reason}`),
        );
    }).on('close', () => {
        clearInterval(interval);
        console.log('WebSocket is shutdown');
    });
}

function setupEndpoints(app: Express): void {
    app.route('/games').get(getGames).post(createGame);
    app.route('/games/:id([0-9]+)').get(getGameById).put(updateGame).delete(deleteGame);
}
