import { Request, Response } from 'express';
import * as GamesService from '../services/games';
import { wss } from '../app';

export async function getGames(req: Request, res: Response): Promise<void> {
    const games = await GamesService.getGames();
    res.status(200).send(games);
}

export async function getGameById(req: Request, res: Response): Promise<void> {
    const game = await GamesService.getGameById(Number(req.params.id));
    game ? res.status(200).send(game) : res.sendStatus(404);
}

export async function createGame(req: Request, res: Response): Promise<void> {
    const game = await GamesService.createGame(req.body);
    setTimeout(() => wss.clients.forEach((client) => client.send(JSON.stringify(game))), 10000);
    res.status(201).send(game);
}

export async function updateGame(req: Request, res: Response): Promise<void> {
    const game = await GamesService.updateGame(Number(req.params.id), req.body);
    game ? res.status(200).send(game) : res.sendStatus(404);
}

export async function deleteGame(req: Request, res: Response): Promise<void> {
    const game = await GamesService.deleteGame(Number(req.params.id));
    game ? res.status(200).send(game) : res.sendStatus(404);
}
