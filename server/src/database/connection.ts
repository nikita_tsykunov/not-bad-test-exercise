import Knex from 'knex';
import knexStringcase from 'knex-stringcase';

export const config = {
    default: {
        client: 'postgresql',
        connection: {
            host: 'postgres',
            database: 'postgres',
            user: 'postgres',
            password: 'postgres',
            port: 54321,
        },
    },
    dev: {
        client: 'postgresql',
        connection: {
            database: 'casino',
            user: 'postgres',
            password: 'postgres',
            port: 5432,
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: 'dist/database/migrations',
        },
        seeds: {
            directory: 'dist/database/seeds',
        },
        debug: true,
    },
    test: {
        client: 'postgresql',
        connection: {
            database: 'casino_test',
            user: 'postgres',
            password: 'postgres',
            port: 5432,
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: 'src/database/migrations',
        },
        seeds: {
            directory: 'src/database/seeds',
        },
    },
    production: {
        client: 'postgresql',
        connection: {
            host: 'postgres',
            database: 'casino',
            user: 'postgres',
            password: 'postgres',
            port: 54321,
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: 'dist/database/migrations',
        },
        seeds: {
            directory: 'dist/database/seeds',
        },
    },
};

export const knex = Knex(
    knexStringcase({
        ...config[process.env.NODE_ENV],
        recursiveStringcase: (value, name: string) => typeof value === 'object' && name === 'root.rows',
    }),
);

export async function createDatabase(): Promise<void> {
    const connection = config[process.env.NODE_ENV].connection;
    const knex = Knex(config.default);
    await knex.raw('CREATE DATABASE ??', [connection.database]).catch((error) => {
        if (!error.message.includes('already exists')) {
            console.error(error);
        }
    });
    return knex.destroy();
}

export async function runMigrations(): Promise<void> {
    return knex.migrate.latest();
}

export async function runSeeds(): Promise<Array<string[]>> {
    return knex.seed.run();
}
