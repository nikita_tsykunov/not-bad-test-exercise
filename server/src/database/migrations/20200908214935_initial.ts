// import Knex from 'knex';

import Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('games'))) {
        await knex.schema.createTable('games', (tableBuilder) => {
            tableBuilder.specificType('id', 'INT GENERATED ALWAYS AS IDENTITY');
            tableBuilder.text('name');
            tableBuilder.text('description');
            tableBuilder.text('provider');
            tableBuilder.text('image_path');
            tableBuilder.timestamps(true, true);
            tableBuilder.index(['name', 'provider']);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('games');
}
