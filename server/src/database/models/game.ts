export type Game = {
    id: number;
    name: string;
    description?: string;
    provider: string;
    imagePath: string;
    createdAt?: Date;
    updatedAt?: Date;
};
