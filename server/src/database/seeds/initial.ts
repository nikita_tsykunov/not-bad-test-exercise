import * as Knex from 'knex';
import { Game } from '../models/game';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('games').truncate();

    // Inserts seed entries
    await knex<Game>('games').insert([
        { name: 'Starburst', provider: 'Netent', imagePath: '/s/images/starburst.jpg' },
        { name: 'Reactoonz', provider: 'Yggdrasil', imagePath: '/s/images/reactoonz.jpg' },
        { name: 'Wild West', provider: 'Yggdrasil', imagePath: '/s/images/wild_west.jpg' },
        { name: 'Razor Shark', provider: 'Push Gaming', imagePath: '/s/images/razor_shark.jpg' },
        { name: 'Book of Dead', provider: 'Play`n Go', imagePath: '/s/images/book_of_dead.jpg' },
    ]);
}
