import { NextFunction, Request, Response } from 'express';

export function errorHandler(err: Error, req: Request, res: Response, next: NextFunction): any {
    console.error(err);
    res.status(500).json('Internal server error');
    next(err);
}
