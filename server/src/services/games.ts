import { Game } from '../database/models/game';
import { knex } from '../database/connection';

export async function getGames(): Promise<Game[]> {
    return knex<Game>('games').select();
}

export async function getGameById(id: number): Promise<Game> {
    return knex<Game>('games').select().where({ id }).first();
}

export async function createGame(game: Partial<Game>): Promise<Game> {
    const [result] = await knex<Game>('games').insert(game).returning('*');
    return result;
}

export async function updateGame(id: number, game: Game): Promise<Game> {
    const [result] = await knex<Game>('games').update(game).where({ id }).returning('*');
    return result;
}

export async function deleteGame(id: number): Promise<Game> {
    const [result] = await knex<Game>('games').delete().where({ id }).returning('*');
    return result;
}
