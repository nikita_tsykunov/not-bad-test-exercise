import { app } from '../setup';
import chai, { expect } from 'chai';
import { knex } from '../../src/database/connection';

describe('Testing game API', () => {
    let server: ChaiHttp.Agent;
    const game = { name: 'Test Name', provider: 'Test Provider', imagePath: 'path' };
    const gameUpdate = { ...game, name: 'Cool Name' };
    before(() => {
        server = chai.request(app).keepOpen();
    });
    after(async () => {
        server.close();
        return knex('games').truncate();
    });

    describe('When request all games with empty database', () => {
        it('Then it should return empty array', () => {
            return server.get('/games').then((response) => {
                expect(response).to.have.status(200);
                expect(response.body).to.be.an('array').and.be.empty;
            });
        });
    });

    describe('When creating new game', () => {
        it('Then it should return just created game', () => {
            return server
                .post('/games')
                .type('json')
                .send(game)
                .then((response) => {
                    expect(response).to.have.status(201);
                    expect(response.body)
                        .to.be.an('object')
                        .and.to.satisfy(
                            (newGame) =>
                                game.name === newGame.name &&
                                game.provider === newGame.provider &&
                                game.imagePath === newGame.imagePath,
                        );
                });
        });
    });

    describe('When requesting game by id', () => {
        describe("When game doesn't exist", () => {
            it('Then it should return 404 response', () => {
                return server.get('/games/2').then((response) => {
                    expect(response).to.have.status(404);
                    expect(response.body).to.be.empty;
                });
            });
        });
        describe('When game exists', () => {
            it('Then it should return game', () => {
                return server.get('/games/1').then((response) => {
                    expect(response).to.have.status(200);
                    expect(response.body)
                        .to.be.an('object')
                        .and.to.satisfy(
                            (newGame) =>
                                game.name === newGame.name &&
                                game.provider === newGame.provider &&
                                game.imagePath === newGame.imagePath,
                        );
                });
            });
        });
    });

    describe('When updating game', () => {
        describe("When game doesn't exist", () => {
            it('Then it should return 404 response', () => {
                return server
                    .put('/games/2')
                    .type('json')
                    .send(gameUpdate)
                    .then((response) => {
                        expect(response).to.have.status(404);
                        expect(response.body).to.be.empty;
                    });
            });
        });
        describe('When game exists', () => {
            it('Then it should return updated game', () => {
                return server
                    .put('/games/1')
                    .type('json')
                    .send(gameUpdate)
                    .then((response) => {
                        expect(response).to.have.status(200);
                        expect(response.body)
                            .to.be.an('object')
                            .and.to.satisfy(
                                (newGame) =>
                                    gameUpdate.name === newGame.name &&
                                    gameUpdate.provider === newGame.provider &&
                                    gameUpdate.imagePath === newGame.imagePath,
                            );
                    });
            });
        });
    });

    describe('When deleting game', () => {
        describe("When game doesn't exist", () => {
            it('Then it should return 404 response', () => {
                return server.delete('/games/2').then((response) => {
                    expect(response).to.have.status(404);
                    expect(response.body).to.be.empty;
                });
            });
        });
        describe('When game exists', () => {
            it('Then it should return deleted game', () => {
                return server.delete('/games/1').then((response) => {
                    expect(response).to.have.status(200);
                    expect(response.body)
                        .to.be.an('object')
                        .and.to.satisfy(
                            (newGame) =>
                                gameUpdate.name === newGame.name &&
                                gameUpdate.provider === newGame.provider &&
                                gameUpdate.imagePath === newGame.imagePath,
                        );
                });
            });
        });
    });
});
