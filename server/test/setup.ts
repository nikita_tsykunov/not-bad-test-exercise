import chaiHttp from 'chai-http';
import chai from 'chai';
import Knex from 'knex';
import { config, createDatabase, knex, runMigrations } from '../src/database/connection';
import { setupExpress, wss } from '../src/app';
import { Server } from 'http';

chai.use(chaiHttp);
export let app: Server;

before(async () => {
    const knex = Knex(config.default);
    await knex.raw('DROP DATABASE IF EXISTS ??', [config.test.connection.database]);
    await knex.destroy();
    await createDatabase();
    await runMigrations();
    return new Promise((resolve) => (app = setupExpress(resolve)));
});
after(async () => {
    wss.close();
    await knex.destroy();
});
