import WebSocket from 'ws';
import chai, { expect } from 'chai';
import { app } from '../setup';
import { knex } from '../../src/database/connection';

describe.only('Testing game websocket', () => {
    let ws: WebSocket;
    let server: ChaiHttp.Agent;
    const game = { name: 'Test Name', provider: 'Test Provider', imagePath: 'path' };
    before(() => {
        ws = new WebSocket('ws://localhost:3000/ws/games');
        server = chai.request(app).keepOpen();
    });
    after(async () => {
        server.close();
        return knex('games').truncate();
    });
    describe('When new game is created', () => {
        it('Then it should send new game via websocket', (done) => {
            ws.on('message', (message) => {
                expect(message).to.be.a('string');
                expect(JSON.parse(message.toString())).to.satisfy(
                    (newGame) =>
                        game.name === newGame.name &&
                        game.provider === newGame.provider &&
                        game.imagePath === newGame.imagePath,
                );
                done();
            });
            server.post('/games').type('json').send(game).catch(console.error);
        });
    });
});
